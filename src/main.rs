#![feature(process_exitcode_placeholder)]
#![feature(termination_trait_lib)]
#![feature(never_type)]

use std::env::args;
use std::process::{Command, ExitCode, ExitStatus, Stdio, Termination};
#[cfg(feature = "regex")]
use regex::Regex;

fn main() -> ResultWrapper {
    let args = args().skip(1);
    let bin = env!("QEMU_BIN");
    let elf_path = env!("QEMU_ELF_INTERPRETER_PATH");
    let envs = std::env::vars();

    #[cfg(feature = "regex")]
    let ignore_pattern = option_env!("QEMU_WRAPPER_ENV_IGNORE_PATTERN").map(|s| Regex::new(s).unwrap());
    #[cfg(feature = "regex")]
    let ignore_env_filter = |kv: &(String, String)| {
        let (k, _) = kv;

        if let Some(ref ignore_pt) = ignore_pattern {
            ignore_pt.is_match(k)
        } else {
            false
        }
    };

    #[cfg(feature = "regex")]
    let include_pattern = option_env!("QEMU_WRAPPER_ENV_INCLUDE_PATTERN").map(|s| Regex::new(s).unwrap());
    #[cfg(feature = "regex")]
    let include_env_filter = |kv: &(String, String)| {
        let (k, _) = kv;

        if let Some(ref include_pt) = include_pattern {
            include_pt.is_match(k)
        } else {
            true
        }
    };

    #[cfg(not(feature = "regex"))]
    let ignore_env_filter = |_: &(String, String)| {
        false
    };

    #[cfg(not(feature = "regex"))]
    let include_env_filter = |_: &(String, String)| {
        true
    };

    let env_args = envs
        .filter(|(k, v)| !k.is_empty() && !v.is_empty())
        // This is a little hacky hack, years ago, a QEMU patch changed how -E and -U works
        // and applied the same logic as for QEMU_SET_ENV/QEMU_UNSET_ENV, which allows multiple
        // env vars to be passed in this way: a=b,b=c
        // However, the parser logic does not take into consideration the possibility of env keys or values
        // having a comma in them, like a,b=c or a=b,c, even if they are provided like "a,b=c" or "a=b,c".
        // Some changes was proposed in 2013 and 2015 to restore the original behavior, but they never
        // were merged, and never got so much attention by that time.
        // Providing env variables this way causes QEMU to print the help text instead of running the binary
        // Because of this, we are filtering out all environment variables that has comma, since
        // partially providing them may be more harmful than they entire absence, as we are unable to predict
        // the behavior of applications that expects more than one value in env but only finds one.
        // If you have any trouble with other variables causing the same behavior and not launching the binary,
        // you can simple compile this wrapper with regex feature enabled and
        // passing QEMU_WRAPPER_ENV_IGNORE_PATTERN or QEMU_WRAPPER_ENV_INCLUDE_PATTERN environment variables
        .filter(|(k, v)| !k.contains(",") && !v.contains(","))
        .filter(|kv| !ignore_env_filter(kv))
        .filter(|kv| include_env_filter(kv))
        .map(|(k, v)| vec!["-E".to_string(), format!("{:?}={:?}", k, v)])
        .flatten()
        .collect::<Vec<_>>();

    let spawn = Command::new(format!("{}", bin))
        .args(env_args)
        .arg("-L")
        .arg(elf_path)
        .args(args)
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn();

    let r = match spawn {
        Ok(mut s) => s.wait()
            .map(|e| e.code())
            .map(|e| e.map(|i| ExitCode::from(i as u8)).unwrap_or(ExitCode::SUCCESS)),
        Err(e) => Err(e)
    };

    ResultWrapper(r)
}


struct ResultWrapper(std::io::Result<ExitCode>);

impl Termination for ResultWrapper {
    #[inline]
    fn report(self) -> ExitCode {
        match self.0 {
            Ok(k) => {
                k.report()
            }
            Err(err) => {
                Err::<!, _>(err).report()
            }
        }
    }
}
