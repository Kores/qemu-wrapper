#!/usr/bin/env bash

while IFS="" read -r p || [ -n "$p" ]
do
  ./build-dyn.sh "/usr/bin/$p"
done < archs