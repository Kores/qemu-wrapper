#!/usr/bin/env bash
if [ -z "$1" ]
  then
    echo "Usage: ./build-dyn.sh [QEMU_BINARY_PATH]"
    echo ""
    echo "Example: ./build-dyn.sh /usr/bin/qemu-riscv64"
    echo ""
    exit 1
fi

export QEMU_BIN="$1"
export QEMU_BIN_NAME=${QEMU_BIN##*/}
export QEMU_ARCH=${QEMU_BIN_NAME#"qemu-"}
export QEMU_ARCH=${QEMU_ARCH%%"-"*}
export QEMU_ELF_INTERPRETER_PATH="/usr/$QEMU_ARCH-linux-elf"
if [ -z "$TOOLCHAIN" ]
then
  cargo build --release
else
  cargo "$TOOLCHAIN" build --release
fi
mkdir -p wrapper-out

if [ -f "./target/release/qemu-wrapper" ]
then
  cp "./target/release/qemu-wrapper" "./wrapper-out/$QEMU_BIN_NAME-wrapper"
else
  cp ./target/*/release/qemu-wrapper "./wrapper-out/$QEMU_BIN_NAME-wrapper"
fi
