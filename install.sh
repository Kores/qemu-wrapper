#!/usr/bin/env bash
if [ -z "$1" ] || [ -z "$2" ]
  then
    echo "Usage: ./install.sh [QEMU_BINARY_PATH] [QEMU_ELF_INTERPRETER_PATH]"
    echo ""
    echo "Example: ./install.sh /usr/bin/qemu-riscv64 /usr/riscv64-linux-gnu/"
    echo ""
    exit 1
fi

export QEMU_BIN="$1"
export QEMU_ELF_INTERPRETER_PATH="$2"

if [ -z "$TOOLCHAIN" ]
then
  cargo build --release
else
  cargo "$TOOLCHAIN" build --release
fi

sudo install -Dm 755 "./target/release/qemu-wrapper" "/usr/bin/${QEMU_BIN##*/}-wrapper"